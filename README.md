Easy Implementation of Queue using C++ 
=======================================
This is an Efficient method of implementation of Queue in c++.<br/>
A queue can be implemented using two stacks. <br/>
Let queue to be implemented be q and stacks used to implement q be stack1 and stack2.
<br/>
Justine Almeda - 
Coder at http://applianceauthority.org/air-fryers/air-fryers-shopping-guide-for-2015/
<br/>
=========================================<br/>
enQueue(q, x)
  1) While stack1 is not empty, push everything from satck1 to stack2.
  2) Push x to stack1 (assuming size of stacks is unlimited).
  3) Push everything back to stack1.

dnQueue(q)
  1) If stack1 is empty then error
  2) Pop an item from stack1 and return it